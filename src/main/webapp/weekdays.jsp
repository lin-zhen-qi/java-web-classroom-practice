<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>根据参数请求显示不同页面</title>
</head>
<body>
<c:if test="${param.day eq 'mon'}">
    <p>周一了：工作的第一天，加油！</p>
</c:if>
<c:if test="${param.day eq 'tue'}">
    <p>周二了：工作的第二天，加油！</p>
</c:if>
<c:if test="${param.day eq 'wed'}">
    <p>周三了：工作的第三天，加油！</p>
</c:if>
<c:if test="${param.day eq 'thu'}">
    <p>周四了：工作的第四天，加油！</p>
</c:if>
<c:if test="${param.day eq 'fri'}">
    <p>周五了：工作的第五天，加油！</p>
</c:if>
<c:if test="${param.day eq 'sat'}">
    <p>周六了：休息的第一天</p>
</c:if>
<c:if test="${param.day eq 'sun'}">
    <p>周日了：休息的第二天</p>
</c:if>
<c:if test="${empty param.day}">
    <p>请输入正确的参数（mon/tue/wed/thu/fri/sat/sun）</p>
</c:if>
</body>
</html>