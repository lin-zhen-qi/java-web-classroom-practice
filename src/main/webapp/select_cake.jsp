<%--
  Created by IntelliJ IDEA.
  User: 78496
  Date: 2024/4/4
<%--
  Created by IntelliJ IDEA.
  User: 78496
  Date: 2024/4/4
  Time: 13:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="object.Cake" %>

<html>
<head>
    <title>选购蛋糕</title>
    <style>
        /* Add some basic styling for the cake selection form */
        form {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
        }
        .cake-item {
            width: calc(33% - 20px);
            margin: 10px;
            border: 1px solid #ccc;
            box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.1);
            padding: 10px;
            cursor: pointer;
        }
        .cake-item:hover {
            background-color: #f2f2f2;
        }
        .cake-name, .cake-price {
            margin-top: 10px;
            text-align: center;
        }
    </style>
</head>
<body>

<section class="cake-gallery">
    <h2>蛋糕精选</h2>
    <div>
        <c:forEach items="${sessionScope.cart}" var="cake">
            <form action="/SC" method="post">
                <div class="cake-item">
                    <!-- 蛋糕名称 -->
                    <h3>${cake.name}</h3>
                    <span class="price">${cake.price}</span>
                    <!-- 数量输入框 -->
                    <label for="quantity_${cake.id}">数量：</label>
                    <input type="number" id="quantity_${cake.id}" name="cakeNumber" min="1" max="${cake.stock}" value="1">
                    <!-- 加入购物车按钮 -->
                    <button type="submit">加入购物车</button>
                    <!-- 隐藏字段，用于传递蛋糕ID -->
                    <input type="hidden" name="cakeId" value="${cake.id}">
                    <input type="hidden" name="cakeName" value="${cake.name}">
                    <input type="hidden" name="cakePrice" value="${cake.price}">
                </div>
            </form>
        </c:forEach>
    </div>
</section>

<p><a href="cart.jsp">查看购物车</a></p>

</body>
</html>
