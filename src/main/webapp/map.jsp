<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>显示Map键值对</title>
</head>
<body>

<h1>Map键值对展示</h1>

<h2>Request域中的Map:</h2>
<ul>
    <c:forEach var="entry" items="${requestScope.requestMap}">
        <li>${entry.key}: ${entry.value}</li>
    </c:forEach>
</ul>

<h2>Session域中的Map:</h2>
<ul>
    <c:forEach var="entry" items="${sessionMap}">
        <li>${entry.key}: ${entry.value}</li>
    </c:forEach>
</ul>

<h2>ServletContext域中的Map:</h2>
<ul>
    <c:forEach var="entry" items="${contextMap}">
        <li>${entry.key}: ${entry.value}</li>
    </c:forEach>
</ul>

<!-- 通过JavaScript添加简单的异常处理和空集合时的友好提示 -->
<script>
    function addMessage(elementId, message) {
        var element = document.getElementById(elementId);
        if (element) {
            element.innerHTML = message;
        }
    }

    window.onload = function() {
        if (!${requestScope.requestMap}) {
            addMessage('requestMapMessage', 'Request Map为空');
        }
        if (!${sessionMap}) {
            addMessage('sessionMapMessage', 'Session Map为空');
        }
        if (!${contextMap}) {
            addMessage('contextMapMessage', 'ServletContext Map为空');
        }
    };
</script>

<!-- 使用CSS改善页面布局（需要在<head>中引入或定义样式） -->
<style>
    .map-container {
        margin-bottom: 20px;
    }
    .map-container h2 {
        margin-top: 0;
    }
</style>

</body>
</html>
