<%--
  Created by IntelliJ IDEA.
  User: 78496
  Date: 2024/4/6
  Time: 20:21
  To change this template use File | Settings | File Templates.
--%>
<!-- hello.jsp -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>JSP Example</title>
    <script>
        // 保持原有的JavaScript逻辑不变
        function greet() {
            return "Hello";
        }
    </script>
</head>
<body>
<h1>Welcome to our JSP page!</h1>
<p id="greeting"></p>

<script>
    // 使用AJAX请求来获取用户名，以减少直接在JSP中嵌入Java代码的做法
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "UserServlet", true);
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var name = this.responseText;
            document.getElementById("greeting").innerHTML = greet() + ", " + name;
        }
    };
    xhr.send();
</script>
</body>
</html>
