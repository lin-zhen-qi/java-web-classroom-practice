<%--
  Created by IntelliJ IDEA.
  User: 78496
  Date: 2024/4/4
  Time: 13:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="object.Cake" %>

<html>
<head>
    <title>购物车</title>
    <style>
        table {
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        tr:nth-child(even){background-color: #f2f2f2;}
        th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<h1>您的购物车</h1>
<c:if test="${empty cart}">
    <p>购物车为空</p>
</c:if>
<c:if test="${not empty cart}">
    <table>
        <thead>
        <tr>
            <th>蛋糕ID</th>
            <th>蛋糕名称</th>
            <th>蛋糕价格</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="cake" items="${sessionScope.cart}">
            <tr>
                <td>${cake.id}</td>
                <td><c:out value="${cake.name}" escapeXml="true"/></td>
                <td>${cake.price}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>

<form action="/Java_Web_war_exploded/SC" method="post">
    <input type="submit" value="结账">
</form>
</body>
</html>
