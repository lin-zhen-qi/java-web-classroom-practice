package com.example.java_web;

import object.Cake;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShoppingCart", value = "/SC")
public class ShoppingCartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        List<Cake> cart = (List<Cake>) session.getAttribute("cart");

        // 如果cart为空或null，创建一个新的ArrayList作为临时购物车
        if (cart == null) {
            cart = new ArrayList<>();
        }

        // 实例化蛋糕对象
        Cake cake1 = new Cake(1, "巧克力蛋糕", 25.99,0);
        Cake cake2 = new Cake(2, "草莓奶油蛋糕", 28.50,0);
        Cake cake3 = new Cake(3, "抹茶慕斯蛋糕", 32.00,0);

        // 将蛋糕对象添加到购物车列表中
        cart.add(cake1);
        cart.add(cake2);
        cart.add(cake3);

        // （可选）将更新后的购物车重新存回session中
        session.setAttribute("cart", cart);

        // 获取用户选择的蛋糕信息，这里假设通过请求参数获取
        int cakeId = Integer.parseInt(request.getParameter("cakeId"));
        String cakeName = request.getParameter("cakeName");
        double cakePrice = Double.parseDouble(request.getParameter("cakePrice"));
        int cakeNumber = Integer.parseInt(request.getParameter("cakeNumber"));
        Cake cake = new Cake(cakeId, cakeName, cakePrice, 0);

        cart.add(cake);
        session.setAttribute("cart", cart);
        response.sendRedirect("cart.jsp");
    }
}