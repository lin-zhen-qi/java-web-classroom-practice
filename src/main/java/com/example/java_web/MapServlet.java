package com.example.java_web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "mapServlet", value = "/mapServlet")
public class MapServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 创建几个 Map 并向其中添加一些数据
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("key1", "value1");
        requestMap.put("key2", "value2"); // 确保这两行没有被注释或删除



        Map<String, String> sessionMap = new HashMap<>();
        sessionMap.put("key3", "value3");
        sessionMap.put("key4", "value4");

        Map<String, String> contextMap = new HashMap<>();
        contextMap.put("key5", "value5");
        contextMap.put("key6", "value6");

        // 将RequestMap放入request域
        request.setAttribute("requestMap", requestMap);
        HttpSession session = request.getSession();
        session.setAttribute("sessionMap", sessionMap);
        getServletContext().setAttribute("contextMap", contextMap);
        // 转发到 JSP 页面
        request.getRequestDispatcher("src/main/webapp/map.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}