package com.example.java_web;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "Servlet02",
        urlPatterns ="/Servlet02"
)
public class Servlet02 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name=request.getParameter("username");
        String password=request.getParameter("psw");
        System.out.println("用户名"+name);
        System.out.println("密码"+password);
        System.out.println("发送post请求");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String name=request.getParameter("username");
        String password=request.getParameter("psw");
        System.out.println("用户名"+name);
        System.out.println("密码"+password);
        System.out.println("发送get请求");
    }
    }