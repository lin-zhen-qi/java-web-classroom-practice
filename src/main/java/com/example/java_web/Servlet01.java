package com.example.java_web;

import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Servlet01", value = "/Servlet01")
public class Servlet01 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        Cookie[] cookies = request.getCookies();
        String lastVisitTime = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("lastVisitTime")) {
                    lastVisitTime = URLDecoder.decode(cookie.getValue(), "UTF-8");
                    break;
                }
            }
        }
        if (lastVisitTime == null) {
            lastVisitTime = "First time visit";
        }
        Date now = new Date();
        String encodedTime = URLEncoder.encode(now.toString(), "UTF-8");
        Cookie cookie = new Cookie("lastVisitTime", encodedTime);
        cookie.setMaxAge(365 * 24 * 60 * 60); // Cookie有效期为一年
        response.addCookie(cookie);

        response.getWriter().println("<html><head><title>Welcome</title></head><body>");
        response.getWriter().println("<h1>Welcome to our website!</h1>");
        response.getWriter().println("<p>Your last visit time was: " + lastVisitTime + "</p>");
        response.getWriter().println("</body></html>");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}